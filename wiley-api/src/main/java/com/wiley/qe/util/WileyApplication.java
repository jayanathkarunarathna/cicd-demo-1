package com.wiley.qe.util;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.wiley.qe"})
public class WileyApplication {

    public static void main(String[] args) {
        SpringApplication.run(WileyApplication.class, args);
    }
}
