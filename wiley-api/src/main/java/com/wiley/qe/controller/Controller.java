package com.wiley.qe.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class Controller {


    @GetMapping
    public String getWelcomeNote() {
        return "Welcome to Wiley!";
    }

    @GetMapping(value = "name")
    public String getName() {
        return "Jayanath Karunarathna" ;
    }

    @GetMapping(value = "age")
    public String getAge() {
        return "40" ;
    }
}
