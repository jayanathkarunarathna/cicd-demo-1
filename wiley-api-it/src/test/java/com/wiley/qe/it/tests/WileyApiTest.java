package com.wiley.qe.it.tests;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.wiley.qe.it.api.WileyApi;
import com.wiley.qe.it.utils.BaseTest;

public class WileyApiTest extends BaseTest {

    @Autowired
    WileyApi wapi;

    @Test
    public void testWileyApiGreetingMessage() {
        String expected = "Welcome to Wiley!";
        String actual = wapi.getGreeting().body().asString();
        Assert.assertEquals(actual, expected);
    }
    @Test
    public void verifyWileyApiName() {
        String expected = "Jayanath Karunarathna";
        String actual = wapi.getName().body().asString();
        Assert.assertEquals(actual, expected);
    }
    @Test
    public void verifyWileyApiAge() {
        String expected = "40";
        String actual = wapi.getAge().body().asString();
        Assert.assertEquals(actual, expected);
    }
}
