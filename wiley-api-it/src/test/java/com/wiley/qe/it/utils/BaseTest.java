package com.wiley.qe.it.utils;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

import com.wiley.qe.it.util.WileyTestApplication;

@SpringBootTest(classes= WileyTestApplication.class)
public abstract class BaseTest extends AbstractTestNGSpringContextTests {

}
