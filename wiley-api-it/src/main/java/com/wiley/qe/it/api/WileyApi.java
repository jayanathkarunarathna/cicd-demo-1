package com.wiley.qe.it.api;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

@Service
public class WileyApi extends APIBase {

    @Value("${application.url}")
    private String url;

    public Response getGreeting() {
        return super.get(url, Collections.emptyMap(), Collections.emptyMap(), ContentType.JSON.toString(), 200);
    }

    public Response getName() {
        return super.get(url + "/name", Collections.emptyMap(), Collections.emptyMap(), ContentType.JSON.toString(), 200);
    }

    public Response getAge() {
        return super.get(url + "/age", Collections.emptyMap(), Collections.emptyMap(), ContentType.JSON.toString(), 200);
    }
}
