package com.wiley.qe.it.api;

import java.util.Map;
import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.response.Response;

public class APIBase {

    protected Response get(String uri, Map<String, String> headers, Map<String, String> queryParams, String acceptType, int statusCode) {
        return RestAssured.given()
            .config(RestAssuredConfig.config().encoderConfig(EncoderConfig.encoderConfig()
                .appendDefaultContentCharsetToContentTypeIfUndefined(false)))
            .baseUri(uri)
            .headers(headers)
            .accept(acceptType)
            .queryParams(queryParams)
            .get()
            .then()
            .statusCode(statusCode)
            .extract()
            .response();
    }

}
