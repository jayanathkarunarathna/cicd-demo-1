package com.wiley.qe.it.util;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.wiley.qe"})
public class WileyTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(WileyTestApplication.class, args);
    }
}
